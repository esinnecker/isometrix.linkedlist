﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Isometrix.LinkedList.Class
{
    public class List
    {
        public Index head;
        public Index current;
        private int size;

        public List()
        {
            size = 0;
            head = null;
        }
        public void Add(object content)
        {
            size++;

            Index index = new Index(content);

            if (head == null)
            {
                head = index;
            }
            else
            {
                current.NextIndex = index;
            }
            current = index;
        }
        public void PrintList()
        {
            Index tempNode = head;

            while (tempNode != null)
            {
                Console.WriteLine(tempNode.ValueIndex);
                tempNode = tempNode.NextIndex;
            }
        }        
        public bool Delete(int Position)
        {
            Index tempIndex = head;

            Index lastIndex = null;
            int count = 0;

            while (tempIndex != null)
            {
                if (count == Position - 1)
                {
                    lastIndex.NextIndex = tempIndex.NextIndex;
                    return true;
                }
                count++;

                lastIndex = tempIndex;
                tempIndex = tempIndex.NextIndex;
            }
            
            return false;
        }
    }
}
