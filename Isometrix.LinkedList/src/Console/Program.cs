﻿using Isometrix.LinkedList.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApp
{
	class Program
	{
		static void Main(string[] args)
		{
            List list = new List();

        A:
            Console.WriteLine("Select an option bellow\n\n");
            Console.WriteLine("\tTo add elements in list : Press 1");
            Console.WriteLine("\tTo delete element from list : Press 2");
            Console.WriteLine("\tTo display elements of the list : Press 3");

            int option = Convert.ToInt32(Console.ReadLine());
            switch (option)
			{
                case 1:
					{
                        Console.WriteLine("\nPlease, inform how many elements you wants to put in the list");
                        int elements = 0 ;

                        try
						{
                            elements = Convert.ToInt32(Console.ReadLine());
                        }
                        catch(Exception ex)
						{
                            Console.WriteLine("\nPlease, inform a valid value");
                        }
                        //int elements = Convert.ToInt32(Console.ReadLine());

                        for (int i = 1; i <= elements; i++)
                        {
                            Console.WriteLine("Inform the element you want to add in the list");
                            list.Add(Console.ReadLine());
                        }
                        break;
                    }
                case 2:
					{
                        Console.WriteLine("Please, inform the node you which delete");
                        list.Delete(Convert.ToInt32(Console.ReadLine()));
                        break;
					}
                case 3:
					{
                        Console.WriteLine("************ List ************");
                        list.PrintList();
                        break;
					}
			}
            
            goto A;
        }
	}
}
